@extends('layouts.converter')
@section('content')
    <div>
        <router-view convert-route="{{ route('coins.convert') }}" coins-currencies-route="{{route('coins.currencies')}}" night-image="{{ asset('images/SVG/night.svg') }}" light-image="{{ asset('images/SVG/daynight.svg') }}"></router-view>
    </div>
@endsection
