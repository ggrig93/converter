<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CoinsCurrenciesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/convert', [CoinsCurrenciesController::class, 'convert'])->name('coins.convert');
Route::get('/coins-currencies', [CoinsCurrenciesController::class, 'coinsCurrencies'])->name('coins.currencies');

Route::get('/{any}', function () {
    return view('converter.index');
})->where('any', '.*');
