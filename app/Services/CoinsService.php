<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Coin;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class CoinsService
{
    const URL = 'https://api.coinstats.app/';

    /**
     * Get a coin item data
     *
     * @param array $item
     *
     * @return array
     */
    public function saveCoin($item)
    {
        $coin = Coin::updateOrCreate(
            ['slug' => $item->id],
            [
                'name' => $item->name,
                'slug' => $item->id,
                'symbol' => $item->symbol,
                'price' => (float)$item->price,
                'published_at' => $coin->published_at ?? Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        return $coin->save();
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fetchCoins()
    {
        $client = new Client([
            'headers' => [
                'Accepts' => 'application/json',
            ],
        ]);

        $limit = 2000;
        try {
            $endpoint = self::URL . 'public/v1/coins';
            $data = [];
            $skip = 0;

            do {
                $request = $client->get($endpoint, [
                    'query' => [
                        'skip' => $skip,
                        'limit' => $limit,
                    ],
                ]);

                $result = json_decode($request->getBody()->getContents());
                $coins = $result->coins;
                $data = array_merge($data, $coins);
                $skip += $limit;
            } while (
                count($coins) === $limit
            );

            $rows = [];

            foreach ($data as $key => $item) {
                try {
                    $rows[] = $this->saveCoin($item);
                } catch (\Exception $e) {
                    Log::error(['Error' => [
                        'status' => false,
                        'command' => 'Fetch Coins (' . $item->id . ')',
                        'message' => $e->getMessage(),
                        'code' => $e->getCode(),
                        'line' => $e->getLine(),
                        'file' => $e->getFile(),
                    ]]);
                }
            }

        } catch (\Exception $e) {
            Log::error(['Error' => [
                'status' => false,
                'command' => 'Fetch Coins',
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'line' => $e->getLine(),
                'file' => $e->getFile(),
            ]]);
        }

        return ['success' => [
            'status' => true,
        ]];
    }
}
