<?php

namespace App\Contracts;

use Illuminate\Http\Request;

interface CoinsCurrenciesInterface
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function convert(Request $request);

    /**
     * @return mixed
     */
    public function coinsCurrencies();
}
