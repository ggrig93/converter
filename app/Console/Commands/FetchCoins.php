<?php

namespace App\Console\Commands;

use App\Services\CoinsService;
use Illuminate\Console\Command;

class FetchCoins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:coins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch cryptocurrency latest listings from CoinMarketCap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param CoinsService $coinsService
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(CoinsService $coinsService)
    {
        return $coinsService->fetchCoins();
    }
}
