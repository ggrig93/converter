<?php

namespace App\Console\Commands;

use App\Services\CurrenciesService;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class FetchCurrencies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:currencies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch currency latest listings from CryptoNator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param CurrenciesService $currenciesService
     * @return array
     */
    public function handle(CurrenciesService $currenciesService)
    {
        return $currenciesService->fetchCurrencies();
    }

}
