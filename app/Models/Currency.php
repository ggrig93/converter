<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'symbol',
        'price',
        'published_at',
        'slug',
    ];

    protected $casts = [
        'published_at' => 'datetime',
    ];

    protected $table = 'currencies';
}
